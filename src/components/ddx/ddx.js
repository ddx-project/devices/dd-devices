import Basic from "components/ddx/Basic.tmpl_20AD45E7.jsx";
import Sample from "components/ddx/Samples.tmpl_636E68FA.jsx";
import Image from "components/ddx/Image.tmpl_1AC73C98.jsx";
import Protocol from "components/ddx/Protocol.tmpl_2EF80254.jsx";
import Organization from "components/ddx/Organization.tmpl_05E5EEA3";
import Device from "components/ddx/Device.tmpl_4FEB49C9.jsx";
import DDX from "components/ddx/DDX";

export { Basic, Image, Organization, Protocol, Sample, Device, DDX };

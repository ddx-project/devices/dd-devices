import React from "react";

import { Link } from "react-router-dom";

// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";

// core components
import cardStyle from "assets/jss/material-kit-react/views/componentsSections/cardStyle.jsx";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Typography from "@material-ui/core/Typography";
import Link from "@material-ui/core/Link";

import { OipApi } from "oip/OipApi";
import { config } from "ddx.config.js";

const api = new OipApi(config.daemonApiUrl);

class SectionCard extends React.Component {
  state = {
    name: "",
    description: "",
    organizations: [],
    address: "",
    thumbnail: require("assets/img/ddx-placeHolder.png"),
    txid: ""
  };

  peopleHandlerOfOipRef(id) {
    console.log(`Receiving: ${id}`);
    return api.getRecord(id).then(data => {
      console.log(`Data received: ${data}`);
      let name =
        data.results[0].record.details[config.cardInfo.name.tmpl][
          config.cardInfo.name.name
        ];

      if (data.results[0].record.details[config.cardInfo.surname.tmpl]) {
        name +=
          " " +
          data.results[0].record.details[config.cardInfo.surname.tmpl][
            config.cardInfo.surname.name
          ];
      }
      return {
        id,
        name
      };
    });
  }

  componentDidMount() {
    if (this.props.data) {
      console.log(this.props.data);
      const recordInfo = this.props.data.record.details;
      if (recordInfo) {
        const avatarId =
          recordInfo[config.cardInfo.avatarRecord.tmpl][
            config.cardInfo.avatarRecord.name
          ];
        const callAvatar = api.getRecord(avatarId);

        let name =
          recordInfo[config.cardInfo.name.tmpl][config.cardInfo.name.name];

        const description =
          recordInfo[config.cardInfo.description.tmpl][
            config.cardInfo.description.name
          ];

        const txid = this.props.data.meta.txid;

        const organizationsOipRef =
          recordInfo[config.protocolHandler.organizations.tmpl][
            config.protocolHandler.organizations.name
          ];

        if (organizationsOipRef) {
          const organizations = organizationsOipRef.map(
            this.peopleHandlerOfOipRef
          );

          Promise.all(organizations).then(names => {
            this.setState({
              organizations: names
            });
          });
        }

        this.setState({
          name,
          description,
          txid
        });

        callAvatar.then(avatar => {
          const address =
            avatar.results[0].record.details[
              config.imageHandler.thumbnail.tmpl
            ][config.imageHandler.thumbnail.name];
          this.setState({
            thumbnail: `${config.ipfs.apiUrl}${address}`
          });
        });
      }
    }
  }

  render() {
    const { classes } = this.props;
    return (
      <Card className={classes.card} style={{ height: "247px" }}>
        <CardActionArea>
          <Link className={classes.link} to={"/record/" + this.state.txid}>
            <CardMedia
              component="img"
              alt="Record Image"
              className={classes.media}
              height="150"
              image={this.state.thumbnail}
              title={this.state.name}
              href="/record"
            />
            <CardContent>
              <Typography
                noWrap={true}
                variant="body1"
                style={{ fontStyle: "bold" }}
                color="textPrimary"
              >
                {this.state.name}
              </Typography>
              <Typography
                noWrap={true}
                variant="body2"
                style={{ overflowWrap: "break-word" }}
                color="textPrimary"
              >
                {this.state.description}
              </Typography>
              <Typography
                style={{
                  fontSize: "8px",
                  overflowWrap: "break-word",
                  fontStyle: "italic",
                  fontFamily: "monospace",
                }}
                color="textPrimary"
              >
                {this.state.txid}
              </Typography>
            </CardContent>
          </Link>
        </CardActionArea>
      </Card>
    );
  }
}

export default withStyles(cardStyle, {withTheme: true})(SectionCard);
